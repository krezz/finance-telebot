# finance-telebot

```bash
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/24737009/terraform/state/default" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/24737009/terraform/state/default/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/24737009/terraform/state/default/lock" \
    -backend-config="username=krezz" \
    -backend-config="password=token" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5" \
    -reconfigure
```