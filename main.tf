# Configure the Heroku provider
provider "heroku" {
  email   = local.ws.heroku_email
  api_key = local.ws.heroku_api_key
}

resource "heroku_app" "krezz" {
  name   = "krezz"
  stack  = "container"
  region = local.ws.heroku_region
  # acm    = true # Hobby or above tier required for ACM

  sensitive_config_vars = {
    TELEGRAM_TOKEN  = var.TELEGRAM_TOKEN
    FINNHUB_API_KEY = var.FINNHUB_API_KEY
  }
}

resource "heroku_build" "krezz" {
  app = heroku_app.krezz.name

  source {
    path = "."
  }
}

resource "heroku_formation" "krezz" {
  app      = heroku_app.krezz.id
  type     = "web"
  quantity = 1
  size     = "Free"
  depends_on = [
    heroku_build.krezz
  ]
}
