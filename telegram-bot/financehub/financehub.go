package financehub

import (
	"fmt"
	"finance-telebot/logger"
)

func (fh *Finnhub) GetStockPrice(symbol string) (string, error) {
	quote, _, err := fh.Client.Quote(fh.Auth, symbol)
	if err != nil {
		logger.Log.Error(err)
	}

	msg := fmt.Sprintf(`
<b>%s price (USD)</b>
<pre>
| Current price | $%.2f |
| Open price    | $%.2f |
| High price    | $%.2f |
| Low price     | $%.2f |
| Close price   | $%.2f |
</pre>`,
		symbol,
		quote.C,
		quote.O,
		quote.H,
		quote.L,
		quote.Pc,
	)

	return msg, nil
}

func (fh *Finnhub) GetCompanyNews(symbol string) {
	companyNews, _, err := fh.Client.CompanyNews(fh.Auth, symbol, "", "")
	if err != nil {
		logger.Log.Error(err)
	}
	fmt.Printf("%+v\n", companyNews)
}

func (fh *Finnhub) GetRecommendationTrends(symbol string) {
	recommendations, _, err := fh.Client.RecommendationTrends(fh.Auth, symbol)
	if err != nil {
		logger.Log.Error(err)
	}

	trends := recommendations[:4]

	fmt.Printf("%+v\n", trends)

	draw()
}