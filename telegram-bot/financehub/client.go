package financehub

import (
	"context"
	"finance-telebot/config"
	finnhub "github.com/Finnhub-Stock-API/finnhub-go"
)

// Finnhub object
type Finnhub struct {
	Client *finnhub.DefaultApiService
	Auth   context.Context
}

// NewClient Create FinnHub client
func NewClient() *Finnhub {
	fhClient := finnhub.NewAPIClient(finnhub.NewConfiguration()).DefaultApi
	fhAuth := context.WithValue(context.Background(), finnhub.ContextAPIKey, finnhub.APIKey{
		Key: config.Cfg.FinnhubAPIKey,
	})
	return &Finnhub{
		Client: fhClient,
		Auth: fhAuth,
	}
}