package financehub

import (
	"os"

	"github.com/wcharczuk/go-chart/v2"
	"github.com/wcharczuk/go-chart/v2/drawing"
)

func draw() {
	chart.DefaultBackgroundColor = chart.ColorTransparent
	chart.DefaultCanvasColor = chart.ColorTransparent

	barWidth := 120

	var (
		colorWhite          = drawing.Color{R: 241, G: 241, B: 241, A: 255}
		colorMariner        = drawing.Color{R: 60, G: 100, B: 148, A: 255}
		colorLightSteelBlue = drawing.Color{R: 182, G: 195, B: 220, A: 255}
		colorPoloBlue       = drawing.Color{R: 126, G: 155, B: 200, A: 255}
		colorSteelBlue      = drawing.Color{R: 73, G: 120, B: 177, A: 255}
	)

	stackedBarChart := chart.StackedBarChart{
		Title:      "Quarterly Sales",
		// TitleStyle: chart.StyleShow(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 100,
			},
		},
		Width:      810,
		Height:     500,
		// XAxis:      chart.StyleShow(),
		// YAxis:      chart.StyleShow(),
		BarSpacing: 50,
		Bars: []chart.StackedBar{
			{
				Name:  "Q1",
				Width: barWidth,
				Values: []chart.Value{
					{
						Label: "32K",
						Value: 32,
						Style: chart.Style{
							StrokeWidth: .01,
							FillColor:   colorMariner,
							FontColor:   colorWhite,
						},
					},
					{
						Label: "46K",
						Value: 46,
						Style: chart.Style{
							StrokeWidth: .01,
							FillColor:   colorLightSteelBlue,
							FontColor:   colorWhite,
						},
					},
					{
						Label: "48K",
						Value: 48,
						Style: chart.Style{
							StrokeWidth: .01,
							FillColor:   colorPoloBlue,
							FontColor:   colorWhite,
						},
					},
					{
						Label: "42K",
						Value: 42,
						Style: chart.Style{
							StrokeWidth: .01,
							FillColor:   colorSteelBlue,
							FontColor:   colorWhite,
						},
					},
				},
			},
		},
	}

	pngFile, err := os.Create("output.png")
	if err != nil {
		panic(err)
	}

	if err := stackedBarChart.Render(chart.PNG, pngFile); err != nil {
		panic(err)
	}

	if err := pngFile.Close(); err != nil {
		panic(err)
	}
}