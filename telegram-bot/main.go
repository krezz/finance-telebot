package main

import (
	"finance-telebot/config"
	"finance-telebot/logger"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// Server context
type Server struct {
	Cfg    *config.Config
	Router *echo.Echo
}

// NewServer constructor
func NewServer() *Server {
	cfg := config.Cfg

	return &Server{
		Cfg:    cfg,
		Router: getRouter(),
	}
}

// RunServer API
func (s *Server) RunServer() {
	srv := s.Router
	srv.Server.Addr = fmt.Sprintf("%s:%s", s.Cfg.ServerHost, s.Cfg.ServerPort)

	logger.Log.Infof("Starting web server on port %s...", s.Cfg.ServerPort)
	go srv.Logger.Fatal(srv.StartServer(srv.Server))
}

func getRouter() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())

	e.GET("/ping", func(c echo.Context) error {
		return c.String(http.StatusOK, `{"ping":"pong"}`)
	})

	e.GET("/health", func(c echo.Context) error {
		return c.String(http.StatusOK, "OK")
	})

	return e
}

func main() {
	srv := NewServer()
	go srv.RunServer()
	srv.RunTelebot()
	// debugs()
}

// func debugs() {
// 	fh := financehub.NewClient()
// 	fh.GetRecommendationTrends("TSLA")
// }
