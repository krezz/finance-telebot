package main

import (
	"strings"
	"finance-telebot/logger"
	"finance-telebot/financehub"

	tb "gopkg.in/tucnak/telebot.v2"
)

// RunTelebot server
func (s *Server) RunTelebot() {

	fh := financehub.NewClient()

	b, err := tb.NewBot(tb.Settings{
		// You can also set custom API URL.
		// If field is empty it equals to "https://api.telegram.org".
		// URL: "http://195.129.111.17:8012",

		Token:  s.Cfg.TelegramToken,
		Poller: &tb.LongPoller{Timeout: s.Cfg.TelegramPollerTimeout},
	})

	if err != nil {
		logger.Log.Fatal(err)
		return
	}

	b.Handle("/help", func(m *tb.Message) {
		if !m.Private() {
			return
		}

		b.Send(m.Sender, "Hello!")
	})

	var (
		// Universal markup builders.
		// menu     = &tb.ReplyMarkup{ResizeReplyKeyboard: true}
		stockActions = &tb.ReplyMarkup{ResizeReplyKeyboard: true}

		// Reply buttons.
		// btnHelp     = menu.Text("ℹ Help")
		// btnSettings = menu.Text("⚙ Settings")

		// Inline buttons.
		//
		// Pressing it will cause the client to
		// send the bot a callback.
		//
		// Make sure Unique stays unique as per button kind,
		// as it has to be for callback routing to work.
		//
		getStockPrice = stockActions.Data("💲 Market price", "market_price")
	)

	// menu.Reply(
	// 	menu.Row(btnHelp),
	// )
	stockActions.Inline(
		stockActions.Row(getStockPrice),
	)

	b.Handle(&getStockPrice, func(c *tb.Callback) {
		b.Respond(c, &tb.CallbackResponse{
			ShowAlert: false,
		})

		msg, err := fh.GetStockPrice(c.Message.Text)
		if err != nil {
			logger.Log.Error(err)
		}

		b.Send(c.Sender, msg, tb.ModeHTML)
	})

	b.Handle("/stock", func(m *tb.Message) {
		if m.Payload == "" {
			b.Send(m.Sender, "Enter stock symbol name.\nExample: /stock tsla")
		}

		b.Send(m.Sender, strings.ToUpper(m.Payload), stockActions)
	})

	// b.Handle("/start", func(m *tb.Message) {
	// 	b.Handle("stock", func(m *tb.Message) {
	// 		b.Send(m.Sender, "You entered next text "+m.Text)
	// 	})
	// })

	// b.Handle(tb.OnText, func(m *tb.Message) {
	// 	b.Send(m.Sender, "You entered "+m.Text)
	// })

	logger.Log.Info("Starting telegram bot...")
	b.Start()
}
