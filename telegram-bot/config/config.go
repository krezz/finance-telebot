package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

var (
	// Cfg app global
	Cfg = Get()
)

// Config structure
type Config struct {
	ServerHost            string        `envconfig:"HOST" default:"0.0.0.0"`
	ServerPort            string        `envconfig:"PORT" default:"8000"`
	LogLevel              string        `envconfig:"SERVER_LOG_LEVEL" default:"info"`
	ReadTimeout           time.Duration `envconfig:"SERVER_READ_TIMEOUT" default:"10s"`
	WriteTimeout          time.Duration `envconfig:"SERVER_WRITE_TIMEOUT" default:"10s"`
	TelegramToken         string        `envconfig:"TELEGRAM_TOKEN" default:""`
	TelegramPollerTimeout time.Duration `envconfig:"TELEGRAM_POLLER_TIMEOUT" default:"10s"`
	FinnhubAPIKey         string        `envconfig:"FINNHUB_API_KEY" default:""`
}

// Get config
func Get() *Config {
	cfg := &Config{}

	if err := envconfig.Process("", cfg); err != nil {
		panic(err)
	}

	return cfg
}
