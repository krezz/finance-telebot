module finance-telebot

go 1.15

require (
	github.com/Finnhub-Stock-API/finnhub-go v1.2.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kr/text v0.2.0 // indirect
	github.com/labstack/echo/v4 v4.2.0
	github.com/magefile/mage v1.11.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tylerb/graceful v1.2.15
	github.com/wcharczuk/go-chart/v2 v2.1.0
	golang.org/x/sys v0.0.0-20210227040730-b0d1d43c014d // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/tucnak/telebot.v2 v2.3.5
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
